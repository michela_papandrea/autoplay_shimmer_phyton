#!/usr/bin/python
import sys, struct, serial
import time, sched, threading

class myThread(threading.Thread):
    def __init__(self, com):
        threading.Thread.__init__(self)
        self.com = com

    def run(self):
        configureShimmer(self.com)

def wait_for_ack(ser, com):
    ddata = ""
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)
        print "0x%02x" % ord(ddata[0]) + "(" + com + ")"
    return

def stopRecording(ser, com):
    # send stop logging command
    print "STOP command sent to Shimmer (" + com + ")"
    ser.write(struct.pack('B', 0x93))
    wait_for_ack(ser, com)
    ser.close()

def startRecording(ser, com):
    # send start logging command
    print "START command sent to Shimmer (" + com + ")"
    ser.write(struct.pack('B', 0x92))
    wait_for_ack(ser, com)

def configureShimmer(com):
    count = 0
    while 1:
        try:
            ser = serial.Serial(com, 9600)
            ser.flushInput()
        except serial.SerialException as e:
            count += 1
            print
            print "-----------------------------------------"
            print "Serial Exception: " + com + " unable to connect"
            print e
            print "-----------------------------------------"
            print
            # se dopo 5 volte non riesce a connettersi esce dal ciclo
            if count <= 5:
                continue
            else:
                break
        else:
            print
            print "---------------------"
            print com + " opening done..."
            print "---------------------"
            print

            # send the set sensors command
            print "Setting Acc_LN, Acc_WR, Gyro, Mag sensors on Shimmer (" + com + ")"
            ser.write(struct.pack('BBBB', 0x08, 0xE0, 0x10, 0x00))
            wait_for_ack(ser, com)

            # send the set sampling rate command
            # 100,21 Hz (32768 / Hz = 0x0147)
            print "Setting SAMPLING RATE on Shimmer (" + com + ")"
            ser.write(struct.pack('BBB', 0x05, 0x47, 0x01))
            wait_for_ack(ser, com)

            # send the set_expid_command
            print "Setting EXP_ID's on Shimmer (" + com + ")"
            ser.write(struct.pack('BBccccc', 0x7C, 0x05, 'p', 'r', 'o', 'v', 'a'))
            wait_for_ack(ser, com)

            # send the set_rwc_command (8 byte)
            # 5.4.2018 11:36 (UTC)
            # UNIX time sec (1522928198) x 32768 = 49903311192064 = 0x2D6305230000 (HEX)
            # ser.write(struct.pack('BBBBBBBBB', 0x8F, 0x00, 0x00, 0x23, 0x05, 0x63, 0x2D, 0x00, 0x00))
            unix_time_sec = int(time.time())
            crystal_time = unix_time_sec * 32768
            hct = hex(crystal_time).rstrip("L")
            print "Setting TIMESTAMP on Shimmer (" + com + ")"
            ser.write(
                struct.pack('BBBBBBBBB', 0x8F, int(hct[12:14], 16), int(hct[10:12], 16), int(hct[8:10], 16),
                            int(hct[6:8], 16), int(hct[4:6], 16), int(hct[2:4], 16), 0x00, 0x00))
            wait_for_ack(ser, com)

            # start sampling
            startRecording(ser, com)

            # set a timer to 10sec, then stop sampling
            s = sched.scheduler(time.time, time.sleep)
            s.enter(30, 1, stopRecording, (ser, com))
            s.run()
            break

if len(sys.argv) < 2:
    print
    print "No device specified!"
    print
    print "You need to specify the serial port of the devices you wish to connect to"
    print "-> Example:"
    print "   multipleShimmerBTSampling.py Com8 Com10"
    print
else:
    threads = []
    args = len(sys.argv)
    try:
        for i in range(1, args):
            com = sys.argv[i]
            t = myThread(com)
            threads.append(t)

        for t in threads:
            t.start()

        for t in threads:
            t.join()
    except Exception as e:
        print "Error, unable to start thread: " + e

    else:
        print
        print "All done!"
        print
        sys.exit()