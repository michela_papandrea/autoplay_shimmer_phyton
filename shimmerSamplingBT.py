#!/usr/bin/python
import sys, struct, serial
import time, sched

def wait_for_ack():
    ddata = ""
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)
        print "0x%02x" % ord(ddata[0])
    return

def wait_for_bytes():
    bdata = []
    while len(bdata) == 0:
        bdata = ser.read(3)
        print "0x%02x" % ord(bdata[0])
        print "0x%02x" % ord(bdata[1])
        print "0x%02x" % ord(bdata[2])
    return bdata

def stopRecording():
    # send stop logging command
    ser.write(struct.pack('B', 0x93))
    print
    print "STOP command sent, waiting for ACK_COMMAND"
    wait_for_ack()
    print "ACK_COMMAND received."
    # close serial port
    ser.close()
    print "All done"
    sys.exit()

if len(sys.argv) < 2:
    print "no device specified"
    print "You need to specify the serial port of the device you wish to connect to"
    print "example:"
    print "   aAccel5Hz.py Com12"
    print "or"
    print "   aAccel5Hz.py /dev/rfcomm0"
else:
    ser = serial.Serial(sys.argv[1], 115200)
    ser.flushInput()
    print "port opening, done."

    # send the get_trial_config_command
    # ser.write(struct.pack('B', 0x75))
    # bdata = wait_for_bytes()
    # print "LOGGING METHOD reading, done."

    # send the set_trial_config_command (logging method)
    # ser.write(struct.pack('BBBB', 0x73, 0x39, 0x00, 0x00))
    # wait_for_ack()
    # print "LOGGING METHOD setting, done."

    # send the set sensors command
    ser.write(struct.pack('BBBB', 0x08, 0xE0, 0x10, 0x00))
    wait_for_ack()
    print "accLN, accWR, gyro, mag sensor setting, done."

    # send the set sampling rate command
    # 100,21 Hz (32768 / Hz = 0x0147)
    ser.write(struct.pack('BBB', 0x05, 0x47, 0x01))
    wait_for_ack()
    print "SAMPLING RATE setting, done."

    # send the set_expid_command
    ser.write(struct.pack('BBcccccc', 0x7C, 0x06, 'p', 'r', 'o', 'v', 'a', '1'))
    wait_for_ack()
    print "EXP_ID setting, done."

    # send the set_rwc_command (8 byte)
    # 5.4.2018 11:36 (UTC)
    # UNIX time sec (1522928198) x 32768 = 49903311192064 = 0x2D6305230000 (HEX)
    # ser.write(struct.pack('BBBBBBBBB', 0x8F, 0x00, 0x00, 0x23, 0x05, 0x63, 0x2D, 0x00, 0x00))

    unix_time_sec = int(time.time())
    crystal_time = unix_time_sec*32768
    hct = hex(crystal_time).rstrip("L")

    ser.write(struct.pack('BBBBBBBBB', 0x8F, int(hct[12:14], 16), int(hct[10:12], 16), int(hct[8:10], 16), int(hct[6:8], 16), int(hct[4:6], 16), int(hct[2:4], 16), 0x00, 0x00))
    wait_for_ack()
    print "TIMESTAMP configuration, done."

    # send start logging command
    # dt = datetime.now()
    # print('START TIME: {} '.format(dt.microsecond))
    ser.write(struct.pack('B', 0x92))
    wait_for_ack()
    print "START command sending, done."

    # set a timer to 10sec, then stopTheRecording
    s = sched.scheduler(time.time, time.sleep)
    s.enter(10, 1, stopRecording, ())
    s.run()
